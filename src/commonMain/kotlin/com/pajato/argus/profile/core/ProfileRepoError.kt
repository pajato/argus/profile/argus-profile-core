package com.pajato.argus.profile.core

public class ProfileRepoError(message: String) : Exception(message)
