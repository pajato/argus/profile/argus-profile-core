package com.pajato.argus.profile.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class ProfileUnitTest : ReportingTestProfiler() {
    @Test fun `When a default profile is created, verify the values`() {
        val profile = Profile(id = 100, label = "Fred", iconUrl = "")
        assertEquals(100, profile.id)
        assertEquals("Fred", profile.label)
        assertEquals("", profile.iconUrl)
    }

    @Test fun `When serializing a profile object, verify the result`() {
        val data = Profile(id = 1, label = "Argus", iconUrl = "argusAndroidIcon.jpeg")
        val json = """{"id":1,"label":"Argus","iconUrl":"argusAndroidIcon.jpeg"}"""
        assertEquals(json, Json.encodeToString(data))
        assertEquals(data, Json.decodeFromString(json))
    }
}
