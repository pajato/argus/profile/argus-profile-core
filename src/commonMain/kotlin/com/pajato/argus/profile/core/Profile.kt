package com.pajato.argus.profile.core

import kotlinx.serialization.Serializable

@Serializable public data class Profile(val id: Int, val label: String = "", val iconUrl: String = "")
