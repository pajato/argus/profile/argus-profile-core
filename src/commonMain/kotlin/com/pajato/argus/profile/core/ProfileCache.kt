package com.pajato.argus.profile.core

public typealias ProfileCache = MutableMap<Int, SelectableProfile>
