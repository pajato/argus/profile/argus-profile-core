package com.pajato.argus.profile.core

import com.pajato.persister.jsonFormat
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class SelectableProfileUnitTest : ReportingTestProfiler() {
    @Test fun `When serializing and deserializing a profile, verify behavior`() {
        val (isSelected, isHidden, profile) = Triple(false, false, Profile(0))
        val selectableProfile = SelectableProfile(isSelected, isHidden, profile)
        val json = jsonFormat.encodeToString(SelectableProfile.serializer(), selectableProfile)
        val deserialized = jsonFormat.decodeFromString(SelectableProfile.serializer(), json)
        assertEquals(selectableProfile, deserialized)
    }
}
