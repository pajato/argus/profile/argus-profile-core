package com.pajato.argus.profile.core

import com.pajato.argus.shared.core.ItemState
import kotlinx.serialization.Serializable

@Serializable public data class SelectableProfile(
    override val isSelected: Boolean,
    override val isHidden: Boolean,
    val profile: Profile,
) : ItemState
