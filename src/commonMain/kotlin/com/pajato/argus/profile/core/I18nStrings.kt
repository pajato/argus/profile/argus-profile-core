package com.pajato.argus.profile.core

import com.pajato.i18n.strings.StringsResource

public object I18nStrings {
    public const val PROFILE_NOT_A_FILE: String = "ProfileNotAFile"
    public const val PROFILE_URI_ERROR: String = "ProfileUriError"

    public fun registerStrings() {
        StringsResource.put(PROFILE_NOT_A_FILE, "The given file with name '{{name}}' is not a valid file!")
        StringsResource.put(PROFILE_URI_ERROR, "No URI has been injected! A valid file URI is required.")
    }
}
