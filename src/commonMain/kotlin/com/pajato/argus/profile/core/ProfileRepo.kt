package com.pajato.argus.profile.core

import java.net.URI

public interface ProfileRepo {
    public val cache: ProfileCache
    public suspend fun injectDependency(uri: URI)
    public fun register(json: String)
    public fun register(item: SelectableProfile)
    public fun toggleHidden(item: SelectableProfile)
    public fun toggleSelected(item: SelectableProfile)
}
