package com.pajato.argus.profile.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class ProfileRepoErrorUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a profile repo error instance, verify behavior`() {
        val message = "some message"
        val profileRepoError = ProfileRepoError(message)
        assertEquals(message, profileRepoError.message)
    }
}
