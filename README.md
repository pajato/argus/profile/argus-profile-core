# argus-profile-core

## Description

This project defines the
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
"Enterprise Business Rules" layer (aka "Core") for the Argus profile feature. It is responsible for defining the
interfaces, classes and top level artifacts supporting video profiles. It exists to specify these interfaces which
are used by outer layers.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the [Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix   | Test Case Name                                                |
|-------------------|---------------------------------------------------------------|
| I18nStrings       | When accessing the localized network strings, verify behavior |
| ProfileRepoError  | When creating a profile repo error instance, verify behavior  |
| Profile           | When a default profile is created, verify the values          |
|                   | When serializing a profile object, verify the result          |
| SelectableProfile | When serializing and deserializing a profile, verify behavior |

### Notes

The single responsibility for this project is to provide the interface definitions used by outer architectural layers for
the profile feature.

The interfaces adapter layer implements these core interfaces.

Specifically, the core profile interfaces are Profile and ProfileRepo.
